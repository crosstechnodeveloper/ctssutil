<?php

namespace CT\API;


class ResponseHttp {
    public $status_code;
    public $body;
    public $headers;
    public $request;

    public function __construct($status_code, $body, $headers, $request) {
        $this->status_code = $status_code;
        $this->body = $body;
        $this->headers = $headers;
        $this->request = $request;
    }
}