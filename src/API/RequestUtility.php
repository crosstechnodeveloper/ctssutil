<?php

namespace CT\API;

use GuzzleHttp\Client;
use CT\API\ResponseHttp;
use Exception;

class RequestUtility {
    /* 
     * @param String $url
     * @param String $method
     * @param mixed $data
     * @return mixed
     */
    //crate guzzle http request
    public static function httpRequest($url, $method = "GET", $options = [], String $cert = "") {
        $client = new Client();
        try {
            $request = $client->request($method, $url, $options);
            return new ResponseHttp($request->getStatusCode(), $request->getBody(), $request->getHeaders(), $request);
        } catch (\Throwable $th) {
            throw new Exception("Error Processing Request", 1, $th);
        }
    }
}