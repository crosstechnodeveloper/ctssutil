<?php declare(strict_types=1);

use CT\API\RequestUtility;
use CT\API\ResponseHttp;
use PHPUnit\Framework\TestCase;

final class RequestTest extends TestCase
{
    public function testBasicRequest(): void
    {
        $this->assertInstanceOf(
            ResponseHttp::class,
            RequestUtility::httpRequest('http://18.142.27.162:3000/')
        );
    }
}